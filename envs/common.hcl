# This will contains common configurations which are shared across environments
locals {
  tags = {
    created_by      = "Terraform"
    orchestrated_by = "Terragrunt"
  }
  project_id                 = ""
  application                = ""
  business_unit              = ""
  system_tier_classification = ""
  cost_center                = ""
  cost_pool                  = ""
}